package com.gfelline.drinks

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.gfelline.drinks.model.Drink
import kotlinx.android.synthetic.main.activity_drinks.*

class DrinksActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drinks)

        val drinks = arrayListOf(
                Drink("Martini", "Martini Beschreibung", R.drawable.martini),
                Drink("Gin Tonic", "Gin Tonic Beschreibung", R.drawable.gintonic),
                Drink("Sour", "Sour Beschreibung", R.drawable.sour),
                Drink("Martini", "Martini Beschreibung", R.drawable.martini),
                Drink("Gin Tonic", "Gin Tonic Beschreibung", R.drawable.gintonic),
                Drink("Sour", "Sour Beschreibung", R.drawable.sour),
                Drink("Martini", "Martini Beschreibung", R.drawable.martini),
                Drink("Gin Tonic", "Gin Tonic Beschreibung", R.drawable.gintonic),
                Drink("Sour", "Sour Beschreibung", R.drawable.sour),
                Drink("Martini", "Martini Beschreibung", R.drawable.martini),
                Drink("Gin Tonic", "Gin Tonic Beschreibung", R.drawable.gintonic),
                Drink("Sour", "Sour Beschreibung", R.drawable.sour),
                Drink("Martini", "Martini Beschreibung", R.drawable.martini),
                Drink("Gin Tonic", "Gin Tonic Beschreibung", R.drawable.gintonic),
                Drink("Sour", "Sour Beschreibung", R.drawable.sour)
        )

        val drinksAdapter = DrinksAdapter(this, drinks)

        drinksListView.adapter = drinksAdapter

        drinksListView.setOnItemClickListener { parent, view, position, id ->
            val intent = Intent(this, DrinkDetailActivity::class.java)
            intent.putExtra("drink", drinks[position])
            startActivity(intent)
        }

    }
}
