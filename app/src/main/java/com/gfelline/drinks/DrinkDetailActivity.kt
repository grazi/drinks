package com.gfelline.drinks

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.gfelline.drinks.model.Drink
import kotlinx.android.synthetic.main.activity_drink_detail.*

class DrinkDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drink_detail)

        val drink = intent.getParcelableExtra<Drink>("drink")

        detailDrinkNameTxt.text = drink.name
        detailDrinkDescTxt.text = drink.description
        detailDrinkPic.setImageResource(drink.imageId   )
    }
}
