package com.gfelline.drinks

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.gfelline.drinks.model.Drink

/**
 * Created by gfelline on 03.12.17.
 */
class DrinksAdapter(context: Context?, drinks: ArrayList<Drink>) : ArrayAdapter<Drink>(context, 0, drinks) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val drink = getItem(position)

        if (convertView == null) {
            view = LayoutInflater.from(context).inflate(R.layout.view_drink, parent, false)
        } else {
            view = convertView
        }

        val drinkPic = view.findViewById<ImageView>(R.id.drinkPic)
        drinkPic.setImageResource(drink.imageId)

        val drinkName = view.findViewById<TextView>(R.id.drinkNameTxt)
        drinkName.text = drink.name

        val drinkDesc = view.findViewById<TextView>(R.id.drinkDescriptionTxt)
        drinkDesc.text = drink.description

        return view
    }

}